#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static const int KiB = 1024;
typedef struct {

    int top;
    char *bufferPointer[20];
    int size[20];
} Stack;

void push(Stack *S, char *bufferPointer, int size) {
    S->top++;
    S->size[S->top] = size;
    S->bufferPointer[S->top] = bufferPointer;
}

char *pop(Stack *S) {
    char *temp = (S->bufferPointer[S->top]);
    S->top--;
    S->size[S->top] = (S->size[S->top]);
    return temp;
}

int isEmpty(Stack *S) {
    if ((S->top) > 0) {
        return 0;
    }
    else return 1;
}

int getSize(Stack *S) {
    return S->size[S->top];
}

char *createNewBuffer(int size) {
    char *bufferPointer = (char *) malloc((size_t)size);
    if (bufferPointer == NULL) {
        perror("\nMemory not allocated\n");
        exit(1);
    }
    return bufferPointer;
}

void reverse(int size, char *bufferPointer) {
    int i;
    for (i = size - 1; i >= 0; i--) {
        printf("%c", bufferPointer[i]);
    }
    free(bufferPointer);
}

void readText(FILE *file) {
    char *tempBuffer = createNewBuffer(KiB);
    Stack *S = (Stack*) createNewBuffer(sizeof(Stack));
    if(S == NULL){
        perror("\nMemory not allocated");
    }
    else {
        S->top = 0;
    }
    int bytesRead = 0;
    while ((bytesRead = (int) fread(tempBuffer, 1, KiB, file)) > 0) {
        char *bufferPointer = createNewBuffer(bytesRead);
        memcpy(bufferPointer, tempBuffer, (size_t) bytesRead);
        push(S, bufferPointer, bytesRead);
    }

    while (!isEmpty(S)) {
        int size = (getSize(S));
        reverse(size, pop(S));
    }
    free(tempBuffer);
    free(S);
}

int main(int argc, char *argv[]) {
    if (argc != 2) {
        printf("Usage: 1 argument stating file to be read\n");
        return 1;
    }
    else {
        FILE *file;
        file = fopen(argv[1], "r");
        if (file == NULL) {
            perror("File not opened\n");
            exit(1);
        }
        else {
            readText(file);
            fclose(file);

        }
    }
    return 0;
}