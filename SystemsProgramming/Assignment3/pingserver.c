
#include <stdio.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <stdlib.h>

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmissing-noreturn"

#define MAXLENGTH 512
#define PORTNUMBER 2222


int createSocket();
typedef struct {
    int counter;
    char msg[508];
} Datagram;


int createSocket() {
    int mysocket;
    mysocket = socket(AF_INET, SOCK_DGRAM, 0);
    if(mysocket < 0){
        perror("socket fail");
        exit(-1);
    };
    return mysocket;
}
char *createNewBuffer(int size) {
    char *bufferPointer = (char *) malloc((size_t)size);
    if (bufferPointer == NULL) {
        perror("\nMemory not allocated\n");
        exit(1);
    }
    return bufferPointer;
}

struct sockaddr_in addNerworkEssentials(){
    struct sockaddr_in serverAddr;
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_addr.s_addr=htonl(INADDR_ANY);
    serverAddr.sin_port=htons(PORTNUMBER);
    return serverAddr;
}

void main(int argc, char *argv[])
{   int err;
    Datagram *datagram;
    datagram = (Datagram*)createNewBuffer(sizeof(Datagram));
    int counter =0;
    int mysocket,n,m;
    struct sockaddr_in serverAddr= addNerworkEssentials();
    struct sockaddr_in clientAddress={0};
    socklen_t len;
    len = sizeof(struct sockaddr_in);
    mysocket =createSocket();

    err=bind(mysocket,(struct sockaddr *)&serverAddr,sizeof(serverAddr));
    if(err<0){
        perror("bind failed");
    }

    while(1)    {

        n = (int) recvfrom(mysocket, datagram, sizeof(*datagram), 0, (struct sockaddr *) &clientAddress, &len);
         if(counter % 5 == 1){datagram->counter=0;}   //enable for counter mis
        if(counter % 7 == 1){sleep(2);}   //---Enable line for timeout
        if (n < 0) {
            perror("Recv failed");
        }
        m = (int) sendto(mysocket, datagram, sizeof(datagram), 0, (struct sockaddr *) &clientAddress, sizeof(clientAddress));

        if (m < 0) {
            perror("Send to failed");
        }
        counter++;
    }
}



