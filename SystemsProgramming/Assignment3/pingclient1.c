#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/time.h>
#define MAXLENGTH 512
#define PORTNUM 2222

typedef struct {
    char msg[508];
    int counter;
} Datagram;
int createSocket();
void sendDgram(int,Datagram*,struct sockaddr_in);
void recvDgram(int, Datagram*, struct sockaddr_in, socklen_t);
char * print_resolv(const char *) ;
char *createNewBuffer(int size) ;
struct sockaddr_in addNerworkEssentials(char*);
void printData(Datagram* datagram,struct timeval start, struct timeval end);
void sendAndReceiveAndPrint(struct timeval start,struct timeval end,int mysocket, Datagram* datagram,struct sockaddr_in destination, socklen_t len);
Datagram* intializeDatagram(char*);


void main(int argc, char *argv[])
{   char* message = "12345678901234567890";
    struct timeval start={0}, end={0};
    char * hostname = argv[1];
    int mysocket;
    socklen_t len;
    len = sizeof(struct sockaddr_in);
    struct sockaddr_in destination=addNerworkEssentials(hostname);
    Datagram *datagram;
    datagram = intializeDatagram(message);
    mysocket = createSocket();
    sendAndReceiveAndPrint(start,end,mysocket,datagram,destination,len);
    free(datagram);
    close(mysocket);

}

Datagram* intializeDatagram(char* message){
    Datagram * datagram;
    datagram = (Datagram*)createNewBuffer(sizeof(Datagram));
    datagram->counter=0;
    memcpy(datagram->msg,message,strlen(message));
    return datagram;
}

void sendAndReceiveAndPrint(struct timeval start,struct timeval end,int mysocket, Datagram* datagram,struct sockaddr_in destination, socklen_t len){
    gettimeofday(&start,NULL);
    sendDgram(mysocket,datagram, destination);          //send correct datagramr
    memset(datagram,0,sizeof(datagram));
    recvDgram(mysocket,datagram, destination, len);
    gettimeofday(&end,NULL);
    printData(datagram, start, end);
}

void printData(Datagram* datagram,struct timeval start, struct timeval end){
    printf("\n%s\n",datagram->msg);
    printf("rountrip time: %ld\n",((end.tv_sec *1000000 + end.tv_usec)-(start.tv_sec *1000000 + start.tv_usec)));
    memset(datagram,0,sizeof(Datagram));
}

struct sockaddr_in addNerworkEssentials(char*hostname){
    struct sockaddr_in serverAddr;
    print_resolv(hostname);
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_addr.s_addr=inet_addr(print_resolv(hostname));
    serverAddr.sin_port=htons(PORTNUM);
    return serverAddr;
}


char *createNewBuffer(int size) {
    char *bufferPointer = (char *) malloc((size_t)size);
    if (bufferPointer == NULL) {
        perror("\nMemory not allocated\n");
        exit(1);
    }
    return bufferPointer;
}
char* print_resolv(const char *name) {
    struct hostent *resolv;
    struct in_addr *addr;
    resolv = gethostbyname(name);
    if (resolv==NULL) {
        printf("Address not found for %s\n",name);
        exit(-1);
    }
    else {
        addr = (struct in_addr*) resolv->h_addr_list[0];
        return inet_ntoa(*addr);
    }
}


void recvDgram(int mysocket, Datagram *datagram, struct sockaddr_in dest, socklen_t  len){

    int err;
    err =(int) recvfrom(mysocket, datagram ,sizeof(datagram),0,(struct sockaddr*) &dest, &len);
    if(err>=0){
    }
    else{
        printf("Somtething went wrong\n");
    }
}

void sendDgram(int mysocket, Datagram *datagram, struct sockaddr_in dest) {
    int err;
    err = (int) sendto(mysocket, datagram,sizeof(datagram),0,(struct sockaddr*) &dest,sizeof(struct sockaddr_in));
    if(err<0){
        perror("send fail");
        exit(-1);
    }
}

int createSocket() {
    int mysocket;
    mysocket = socket(AF_INET, SOCK_DGRAM, 0);
    if(mysocket < 0){
        perror("socket fail");
        exit(-1);
    };
    return mysocket;
}