#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/time.h>

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmissing-noreturn"
#define MAXLENGTH 512
#define PORTNUM 2222

typedef struct {
    int counter;
    char msg[508];
} Datagram;
int createSocket();
void sendDgram(int,Datagram*,struct sockaddr_in);
int recvDgram(int, Datagram*, struct sockaddr_in, socklen_t);
char * print_resolv(const char *) ;
char *createNewBuffer(int size) ;
void printData(Datagram* datagram, struct timeval start, struct timeval end,int counter);
struct sockaddr_in addNerworkEssentials(char*);
Datagram* intializeDatagram(char*,int);

void main(int argc, char *argv[])

{
    struct timeval start, end;
    int counter=0;
    while(1) {

        char *message = "1234567890";
        char *hostname = argv[1];
        int mysocket;
        socklen_t len;
        len = sizeof(struct sockaddr_in);
        struct timeval timeoutDuration = {1, 0};
        struct sockaddr_in destination=addNerworkEssentials(hostname);;
        Datagram *datagram;
        datagram = intializeDatagram(message,counter);
        mysocket = createSocket();
        setsockopt(mysocket, SOL_SOCKET, SO_RCVTIMEO, (char *) &timeoutDuration, sizeof(struct timeval));
        sleep(1);
        gettimeofday(&start, NULL);
        sendDgram(mysocket, datagram, destination);//send correct datagram
        if (0 >= recvDgram(mysocket, datagram, destination, len)) {
            gettimeofday(&end, NULL);
            printData(datagram, start, end,counter);
            counter++;
        }
        else {
            printf("Packet %d:Timeout/lost\n", datagram->counter);
            counter++;
        }
        memset(datagram,0,sizeof(Datagram));
        close(mysocket);
        free(datagram);
    }

}


Datagram* intializeDatagram(char* message,int counter){
    Datagram * datagram;
    datagram = (Datagram*)createNewBuffer(sizeof(Datagram));
    datagram->counter=counter;
    memcpy(datagram->msg,message,strlen(message));
    return datagram;
}


struct sockaddr_in addNerworkEssentials(char*hostname){
    struct sockaddr_in serverAddr;
    print_resolv(hostname);
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_addr.s_addr=inet_addr(print_resolv(hostname));
    serverAddr.sin_port=htons(PORTNUM);
    return serverAddr;
}


void printData(Datagram* datagram,struct timeval start, struct timeval end,int counter){
    if (counter == datagram->counter) {
        printf("Packet %d: %ld microseconds", datagram->counter,
               (end.tv_sec * 1000000 + end.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec));
        printf("         Message received: %s \n", datagram->msg);
    }
    else {
        printf("Packet %d: wrong counter! received counter %d, instead of %d\n", counter,datagram->counter, counter);
    }
}




char *createNewBuffer(int size) {
    char *bufferPointer = (char *) malloc((size_t)size);
    if (bufferPointer == NULL) {
        perror("\nMemory not allocated\n");
        exit(1);
    }
    return bufferPointer;
}
char* print_resolv(const char *name) {
    struct hostent *resolv;
    struct in_addr *addr;
    resolv = gethostbyname(name);
    if (resolv==NULL) {
        printf("Address not found for %s\n",name);
        exit(-1);
    }
    else {
        addr = (struct in_addr*) resolv->h_addr_list[0];
        return inet_ntoa(*addr);
    }
}


int recvDgram(int mysocket, Datagram *datagram, struct sockaddr_in dest, socklen_t  len){
    int err;
    err =(int) recvfrom(mysocket, datagram ,sizeof(datagram),0,(struct sockaddr*) &dest, &len);
    if(err>=0){
        return 0;
    }
    else{
        return 1;
    }
}

void sendDgram(int mysocket, Datagram *datagram, struct sockaddr_in dest) {
    int err;
    err = (int) sendto(mysocket, datagram,sizeof(datagram),0,(struct sockaddr*) &dest,sizeof(struct sockaddr_in));
    if(err<0){
        perror("send fail");
        exit(-1);
    }
}

int createSocket() {
    int mysocket;
    mysocket = socket(AF_INET, SOCK_DGRAM, 0);
    if(mysocket < 0){
        perror("socket fail");
        exit(-1);
    };
    return mysocket;
}
#pragma clang diagnostic pop