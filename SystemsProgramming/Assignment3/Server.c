
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <stdlib.h>


#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmissing-noreturn"

#define MAXLENGTH 512
#define PORTNUMBER 2222


int createSocket();
void sendDgram(int, char*,struct sockaddr_in);
int recvDgram(int, char*, struct sockaddr_in, socklen_t);


int createSocket() {
    int mysocket;
    mysocket = socket(AF_INET, SOCK_DGRAM, 0);
    if(mysocket < 0){
        perror("socket fail");
        exit(-1);
    };
    return mysocket;
}

int recvDgram(int mysocket, char* msg, struct sockaddr_in dest, socklen_t  len){
    int err;
    err =(int) recvfrom(mysocket, msg ,MAXLENGTH,0,(struct sockaddr*) &dest, &len);
    if(err<0){
        perror("receive fail");
        exit(-1);
    }
    return err;
}

void sendDgram(int mysocket,char * msg, struct sockaddr_in dest) {
    int err;
    err = (int) sendto(mysocket, msg,strlen(msg)+1,0,(const struct sockaddr*) &dest,sizeof(struct sockaddr_in));
    if(err<0){
        perror("send fail");
        exit(-1);
    }
}


void main(int argc, char *argv[])
{
    int counter =0;
    int mysocket,n,m;
    struct sockaddr_in severAddress;
    struct sockaddr_in clientAddress={0};
    socklen_t len;
    len = sizeof(struct sockaddr_in);
    char message[MAXLENGTH];
    mysocket =createSocket();
    severAddress.sin_family = AF_INET;
    severAddress.sin_addr.s_addr=htonl(INADDR_ANY);
    severAddress.sin_port=htons(PORTNUMBER);
    bind(mysocket,(struct sockaddr *)&severAddress,sizeof(severAddress));

    while(1)    {

            n = (int) recvfrom(mysocket, message, MAXLENGTH, 0, (struct sockaddr *) &clientAddress, &len);
            if (n < 0) {
                perror("Recv failed");
            }
            if(counter % 2==1){
            m = (int) sendto(mysocket, message, (size_t) n, 0, (struct sockaddr *) &clientAddress,
                             sizeof(clientAddress));
            if (m < 0) {
                perror("Send to failed");
            }
            message[n] = 0;
            printf("Received the following:\n");
            printf("%s\n", message);
            printf("from %s\n", inet_ntoa(severAddress.sin_addr));
        }
        counter++;

    }
}


